from collections import deque
import mathutils
import math

__all__ = ['Grid']

class Node(dict):
    def __missing__(self, key):
        return math.inf

class Grid(dict):

    PROPERTY_ROAD = 'road'
    PROPERTY_TARGET = 'target'

    def __init__(self, *args):
        self.sizing(*args)
        self.targets = []

    def __missing__(self, key):
        return None

    def sizing(self, scale=8, offset=4):
        self.offset = offset
        self.scale = scale

    def toGrid(self, position):
        position = mathutils.Vector(position)
        return tuple(map(
            lambda x: int(x - self.offset) // self.scale + 1,
            position))

    def toWorld(self, x, y, *_):
        return mathutils.Vector((
            *map(lambda x: x * self.scale, (x, y)), 0))

    def closest(self, position):
        node = self[position[:2]]
        return (math.inf if node is None
            else min(node.values()))

    def neighbors(self, x, y, *_):
        return (
            (x+1, y), (x-1, y),
            (x, y+1), (x, y-1),
        )

    def feed(self, objects):
        for object in objects:
            node = self.toGrid(object.worldPosition)[:2]
            if object.get(self.PROPERTY_ROAD, False):
                self[node] = Node()
            if object.get(self.PROPERTY_TARGET, False):
                self.targets.append(node)

    def clearWeighs(self, target):
        for node in self.values():
            node.pop(target, None)

    def buildWeighs(self, target):
        queue = deque([target])
        distance = 0

        while queue:
            node = queue.popleft()
            self[node][target] = distance

            for node in self.neighbors(*node):
                if (self[node] is not None
                    and target not in self[node]):
                    queue.append(node)

            distance += 1

    def buildAll(self, *targets):
        if not targets:
            targets = self.targets

        for target in targets:
            self.buildWeighs(target)
