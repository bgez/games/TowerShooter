from bgez.framework.types import GameObject, Asset

from bgez.framework.objects import Input
from bgez.framework.inputs import ButtonMapping, KeyboardButton, MouseButton
from bgez.framework.inputs import AxisMapping, MouseAxis

from bgez.bases import Mapped
from bgez import utils

from TowerShooter.Entities.Turrets.Turret_Gatling import *
from TowerShooter.Entities.Turrets.Turret_Slow import *
from TowerShooter.Weapons.pistol_1991 import *
from TowerShooter.Weapons.Katana import *
from TowerShooter.Entities import Damageable, Deployable, Usable, Collider

import mathutils
import math
import bgez

__all__ = ['Player']

class Player(Asset, Collider, Damageable):
    REFERENCE = './TowerShooter/Entities/Player/Player:Player'
    REGISTER = True

    SPEED = 0.1
    SPRINT_BONUS = 2
    MAX_HEALTH = 10
    ROTATION_VERTICAL_LIMITS = (math.radians(-85), math.radians(85))
    MOUSE_THRESHOLD = 1e-5

    SLOTS = dict(
        slot1 = Katana,
        slot2 = pistol_1991,
        dequip = None,
    )

    controls_cam = Input('cam_control', lockedMouse=True, enabled=False).add(
        fire = ButtonMapping(
            MouseButton(Input.Keys.LEFTMOUSE),
                ).listen('ONCEON'),
        playercam = ButtonMapping(
            KeyboardButton(Input.Keys.GKEY)),
    )

    controls = Input('player.controls', lockedMouse=True).add(

        forward = ButtonMapping(
            KeyboardButton(Input.Keys.ZKEY),
            KeyboardButton(Input.Keys.WKEY)),
        backward = ButtonMapping(
            KeyboardButton(Input.Keys.SKEY)),
        left = ButtonMapping(
            KeyboardButton(Input.Keys.QKEY),
            KeyboardButton(Input.Keys.AKEY)),
        right = ButtonMapping(
            KeyboardButton(Input.Keys.DKEY)),

        jump = ButtonMapping(
            KeyboardButton(Input.Keys.SPACEKEY)),
        sprint = ButtonMapping(
            KeyboardButton(Input.Keys.LEFTSHIFTKEY)),

        slot1 = ButtonMapping(
            KeyboardButton(Input.Keys.ONEKEY),
                ).listen('ONCEON'),
        slot2 = ButtonMapping(
            KeyboardButton(Input.Keys.TWOKEY),
                ).listen('ONCEON'),
        slot3 = ButtonMapping(
            KeyboardButton(Input.Keys.THREEKEY),
                ).listen('ONCEON'),
        slot4 = ButtonMapping(
            KeyboardButton(Input.Keys.FOURKEY),
                ).listen('ONCEON'),
        dequip = ButtonMapping(
            KeyboardButton(Input.Keys.ACCENTGRAVEKEY),
                ).listen('ONCEON'),
        reload = ButtonMapping(
            KeyboardButton(Input.Keys.RKEY),
                ).listen('ONCEON'),

        fire = ButtonMapping(
            MouseButton(Input.Keys.LEFTMOUSE),
                ).listen('ONCEON'),

        view_x = AxisMapping(MouseAxis('x'), threshold = MOUSE_THRESHOLD),
        view_y = AxisMapping(MouseAxis('y'), threshold = MOUSE_THRESHOLD),

        freecam = ButtonMapping(
            KeyboardButton(Input.Keys.HKEY)),
    )

    def __getattr__(self, attr):
        try:
            return self.childrenRecursive[attr]
        except:
            raise Exception("No child or attribute named %s" % attr)

    def construct(self):
        super().construct(self)
        self.head         = self.getChildrenByName('Head')[0]
        self.hand_snap    = self.getChildrenByName('HandSnap')[0]
        self.back_snap    = self.getChildrenByName('Back_Snap')[0]
        self.holster_snap = self.getChildrenByName('Holster_Snap')[0]

        self.mouse_sensitivity = mathutils.Vector((2.2, 2.2, 0))
        self.__collisions = [None]
        self.items = []
        self.equipped = None
        self.turret_to_deploy = None

        self.gold = 5
        self.placing_range = 6
        self.health = self.MAX_HEALTH

        self.PlayerCamera1.fov = 100

    @Mapped.bind('forward', 'backward', 'left', 'right', 'jump')
    def movement(self, event):
        speed = self.SPEED * (self.SPRINT_BONUS if event.sprint else 1)
        move_local  = mathutils.Vector((0, 0, 0))

        if(event.forward):
            move_local.y += 1
        if(event.backward):
            move_local.y -= 1
        if(event.left):
            move_local.x -= 1
        if(event.right):
            move_local.x += 1

        move_local.normalize()
        self.applyMovement(move_local * speed, True)

        if event.jump:
            collisions = self.get_current_collisions()
            if collisions:
                z_axis = mathutils.Vector((0, 0, -1))
                for object, position, normal in collisions:
                    if (z_axis.angle(normal) < math.radians(45)
                    and abs(self.getLinearVelocity().z) < 1):
                        self.applyMovement((0, 0, 0.1))
                        veloc = self.getLinearVelocity()
                        veloc.z += 6
                        self.setLinearVelocity(veloc)

    @Mapped.bind('view_x', 'view_y')
    def look(self, event):
        if event.view_x:
            self.applyRotation((
                0, 0, event.view_x * self.mouse_sensitivity.x), True)

        if event.view_y:
            self.head.applyRotation((
                event.view_y * self.mouse_sensitivity.y, 0, 0), True)
            orientation = self.head.localOrientation.to_euler()
            orientation.x = utils.clamp(orientation.x,
                self.ROTATION_VERTICAL_LIMITS[0],
                self.ROTATION_VERTICAL_LIMITS[1])
            self.head.localOrientation = orientation

    @Mapped.bind('slot1', 'slot2', 'dequip')
    def equip(self, event):
        if self.turret_to_deploy is not None:
            self.turret_to_deploy.endObject()
            self.turret_to_deploy = None
        if event.slot1:
            if self.equipped is self.items[0][0]:
                self.unequip_weapon(self.equipped)
            else:
                self.unequip_weapon(self.equipped)
                self.equip_weapon(self.items[0][0])

        if event.slot2:
            if self.equipped is self.items[1][0]:
                self.unequip_weapon(self.equipped)
            else:
                self.unequip_weapon(self.equipped)
                self.equip_weapon(self.items[1][0])

        if event.dequip:
            self.unequip_weapon(self.equipped)
        return
        for key in ('slot1', 'slot2', 'dequip'):
            if event[key]:

                equipped = self.equipped
                item = self.SLOTS[key]

                if self.equipped is not None:
                    self.equipped.endObject()
                    self.equipped = None

                if item is not None:
                    if type(equipped) is not item:
                        self.equipped = self.scene.addAsset(
                            item, self.hand_snap)
                        self.equipped.setParent(self.hand_snap)
                        self.equipped.Owner = self

                return
            
    
    @Mapped.bind('slot3')
    def equip_3(self, event):
        self.unequip_weapon(self.equipped)
        if not isinstance(self.turret_to_deploy, Turret_Gatling_BP):
            if self.turret_to_deploy is not None:
                self.turret_to_deploy.endObject()
            self.turret_to_deploy = self.scene.addAsset(Turret_Gatling_BP)
            self.turret_to_deploy.setParent(self.head)
            self.update_turret()
        else:
            self.turret_to_deploy.endObject()
            self.turret_to_deploy = None

    @Mapped.bind('slot4')
    def equip_4(self, event):
        self.unequip_weapon(self.equipped)
        if not isinstance(self.turret_to_deploy, Turret_Slow_BP):
            if self.turret_to_deploy is not None:
                self.turret_to_deploy.endObject()
            self.turret_to_deploy = self.scene.addAsset(Turret_Slow_BP)
            self.turret_to_deploy.setParent(self.head)
            self.update_turret()
        else:
            self.turret_to_deploy.endObject()
            self.turret_to_deploy = None

    @Mapped.bind('reload')
    def reload(self, event):
        if self.equipped is self.items[1][0]:
            self.equipped.reload()
    

    @Mapped.bind('freecam')
    def freecam(self, event):
        self.controls.disable()
        self.controls_cam.enable()
        freecam = self.scene.objects.get('__default__cam__')
        freecam.setCamera()
    
    @Mapped.bind('playercam')
    def playercam(self, event):
        freecam = self.scene.objects.get('__default__cam__')
        freecam.controls.disable()
        self.controls_cam.disable()
        self.controls.enable()
        self.PlayerCamera1.setCamera()

    @Mapped.bind('fire')
    def shoot(self, event):
        if isinstance(self.equipped, Usable):
            self.equipped.use()
        if (isinstance(self.turret_to_deploy, Deployable) and \
        self.gold >= self.turret_to_deploy.price):
            if self.turret_to_deploy.deploy(self):
                self.gold -= self.turret_to_deploy.price

    def post_draw(self, scene):
        if scene is not self.scene: return
        if isinstance(self.equipped, pistol_1991):
            self.equipped.draw_aim_line()
        if self.turret_to_deploy is not None:
            self.update_turret()

    def update_turret(self):
        start = self.head.worldPosition.copy()
        vect = mathutils.Vector((0, self.placing_range, 0))
        end = start + self.head.getAxisVect(vect)
        obj, hit, normal = self.rayCast(end, poly=0, mask=1)

        self.turret_to_deploy.worldPosition = end if hit is None else hit
        self.turret_to_deploy.worldOrientation = mathutils.Vector((0, 0, 1))

    def find_snap(self, weapon):
        for weapon_p in self.items:
            if weapon is weapon_p[0]:
                return weapon_p[1]
    
    def equip_weapon(self, weapon):
        self.equipped = weapon
        self.equipped.setParent(self.hand_snap)
        self.equipped.worldPosition = self.hand_snap.worldPosition.copy()
        self.equipped.worldOrientation = self.hand_snap.worldOrientation.copy()

    def unequip_weapon(self, weapon):
        if weapon is not None:
            snap = self.find_snap(weapon)
            self.equipped.setParent(snap)
            self.equipped.worldPosition = snap.worldPosition.copy()
            self.equipped.worldOrientation = snap.worldOrientation.copy()
            self.equipped = None

    def getAmmo(self):
        return (
            self.equipped.bullets if isinstance(self.equipped, pistol_1991) else ':('
        )

    def hit(self, attacker):
        self.health -= 1
        self.health = max(self.health, 0)
        return True
