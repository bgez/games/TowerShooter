from bgez.framework.types import Asset, GameObject

from .. import Damageable
from TowerShooter.World.Grid import *

import mathutils
import bgez

__all__ = ['Enemy']

class Enemy(Asset, GameObject, Damageable):
    REFERENCE = './TowerShooter/Entities/Enemy/Enemy:Enemy'
    REGISTER = True

    GRID = Grid()

    LERP_ALIGN = .08
    SPEED_BASE = 0.15

    MAX_HEALTH = 60.
    _health = MAX_HEALTH

    @property
    def health(self):
        return self._health

    @health.setter
    def health(self, value):
        self.localScale.normalize()
        self.localScale *= 1 + self.MAX_HEALTH / 200
        self.localScale *= max(value / self.MAX_HEALTH, .3)
        self._health = value

    def construct(self):
        self.range = 6
        self.last_hit = 0
        self.attack_speed = 1
        self.speed = self.SPEED_BASE

    def hit(self, attacker):
        if self.health == 0:
            return None
        self.health -= attacker.damages
        self.health = max(self.health, 0)
        if self.health == 0:
            attacker.owner.gold += 1
            self.endObject()
            return None
        return True

    def post_draw(self, scene):
        if scene is not self.scene: return

        grid = self.GRID
        position = grid.toGrid(self.worldPosition)

        neighbors = [position]
        neighbors.extend(node for node in grid.neighbors(*position)
            if grid[node] is not None)

        next = min(neighbors, key=grid.closest)

        target = grid.toWorld(*next)
        delta = target - self.worldPosition
        self.alignAxisToVect(delta, 1, self.LERP_ALIGN)

        rotation = self.worldOrientation.to_euler()
        rotation.x = rotation.y = 0
        self.worldOrientation = rotation

        movement = mathutils.Vector((0, self.speed, 0))
        if grid.closest(position) == 0:
            pass
            movement.x = self.speed * .05
            # movement.y = 0
            
        frame = bgez.logic.getFrameTime()
        if (self.last_hit + self.attack_speed) <= frame:
            self.last_hit = frame
            self.attack()

        self.applyMovement(movement, True)

    def attack(self):
        start = self.worldPosition.copy()
        target = mathutils.Vector((0, self.range, 0))
        end = start + self.getAxisVect(target)
        obj = self.rayCastTo(end)
        if obj is not None and not isinstance(obj, self.__class__):
            obj.hit(self)
