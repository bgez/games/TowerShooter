from bgez.framework.types import Asset, GameObject
from bgez.framework.objects import Input
from bgez.bases import Mapped
from bgez import utils

from bgez.framework.inputs import ButtonMapping, AxisMapping
from bgez.framework.inputs import MouseButton, MouseAxis, KeyboardButton

from TowerShooter.Entities.Enemy import Enemy
from TowerShooter.Entities import Deployable, Damageable, Collider

import mathutils
import math
import bgez

class Turret_Gatling(Asset, GameObject):
    REGISTER = True
    REFERENCE = './TowerShooter/Entities/Turrets/Turret_Gatling/Turret_Gatling:Turret_Gatling'

    Holder_rot_limit = (math.radians(-10), math.radians(45))
    rot_step = math.radians(5)

    def construct(self):
        self.Armature    = self.getChildrenByName('Turret_armature')[0]
        self.Cam_TAC     = self.getChildrenByName('Cam_TAC')[0]
        self.Ref_Barrels = self.getChildrenByName('Ref_Barrels')[0]
        self.Holder      = self.getChildrenByName('Holder')[0]
        self.Hitbox      = self.getChildrenByName('Turret_Gatling_HB')[0]

        self.Bone_main        = self.Armature.channels['Main']
        self.Bone_holder      = self.Armature.channels['Holder']
        self.Bone_barrels     = self.Armature.channels['Barrels']
        self.Bone_main.rotation_mode     = bgez.logic.ROT_MODE_QUAT
        self.Bone_holder.rotation_mode   = bgez.logic.ROT_MODE_QUAT
        self.Bone_barrels.rotation_mode  = bgez.logic.ROT_MODE_QUAT
        
        self.damages   = 0.5
        self.range   = 15
        self.fire_rate = 0.01
        self.last_shot = 0

        self.target = None
        self.owner  = None
        self.aligned = False
        self.targets_own = []

        self.update_targets()

    def post_draw(self, scene):
        if self.target is None or self.target.invalid or \
        not self.is_in_sight(self.target):
            self.get_target()
        else:
            self.align_to_target()
            frame = bgez.logic.getFrameTime()
            if (self.aligned and (self.last_shot + self.fire_rate) <= frame):
                self.last_shot = frame
                self.fire()

    def move_main(self, angle):
        axis_rot = (0, 1, 0)
        main_angle_quat = mathutils.Quaternion(self.Bone_main.rotation_quaternion)
        main_angle_quat = utils.quatRot(
            main_angle_quat,
            axis_rot,
            angle, True
        )
        self.Bone_main.rotation_quaternion = main_angle_quat
        self.Armature.update()

    def move_holder(self, angle):
        axis_rot = (1, 0, 0)
        holder_angle_quat = mathutils.Quaternion(self.Bone_holder.rotation_quaternion)
        holder_angle_quat = utils.quatRot(
            holder_angle_quat,
            axis_rot,
            angle, True
        )
        next_holder_angle_axis = holder_angle_quat.to_axis_angle()
        next_holder_angle = next_holder_angle_axis[0].x * next_holder_angle_axis[1]
        next_holder_angle = utils.clamp(
            next_holder_angle,
            self.Holder_rot_limit[0],
            self.Holder_rot_limit[1]
        )
        next_holder_angle_axis[0].x = next_holder_angle
        self.Bone_holder.rotation_quaternion = mathutils.Quaternion(next_holder_angle_axis[0])
        self.Armature.update()

    def get_angle_h(self, target):
        target_pos      = target.worldPosition
        turret_pos      = self.Holder.worldPosition
        barrels_ref_pos = self.Ref_Barrels.worldPosition

        vect_ref = mathutils.Vector(
            (barrels_ref_pos.x - turret_pos.x,
            barrels_ref_pos.y - turret_pos.y)
        )
        vect_target = mathutils.Vector(
            (target_pos.x - turret_pos.x,
            target_pos.y - turret_pos.y)
        )
        return vect_target.angle_signed(vect_ref, None)
    
    def get_angle_v(self, target):
        target_pos = target.worldPosition
        turret_pos = self.Holder.worldPosition

        vect_target = target_pos - turret_pos
        vect_ref_0  = mathutils.Vector((vect_target.x, vect_target.y, 0))
        angle_from_0  = vect_ref_0.angle(vect_target)
        angle_from_0 *= -1 if target_pos.z < turret_pos.z else 1

        holder_angle = mathutils.Quaternion(self.Bone_holder.rotation_quaternion).to_axis_angle()
        holder_angle = holder_angle[0].x * holder_angle[1]
        return angle_from_0 - holder_angle

    def align_to_target(self):
        Angle_h = self.get_angle_h(self.target)
        Angle_v = self.get_angle_v(self.target)

        if(abs(Angle_h) > 0.000001):
            step_h = Angle_h  if abs(Angle_h) < self.rot_step else \
                     self.rot_step if Angle_h > 0 else -self.rot_step
            self.move_main(step_h)

        if(abs(Angle_v) > 0.000001):
            step_v = Angle_v  if abs(Angle_v) < self.rot_step else \
                     self.rot_step if Angle_v > 0 else -self.rot_step
            self.move_holder(step_v)
        if(abs(Angle_h) < 0.05 and abs(Angle_v) < 0.05):
            self.aligned = True

    def update_targets(self):
        self.targets_own.clear()
        for enemy in Enemy.GetInstances():
            if self.getDistanceTo(enemy) <= self.range:
                self.targets_own.append(enemy)

    def get_target(self):
        self.update_targets()
        for enemy in self.targets_own:
            if self.is_in_sight(enemy):
                self.target = enemy
                self.aligned = False

    def is_in_sight(self, enemy):
        obj = self.Hitbox.rayCastTo(enemy, self.range)
        return obj is enemy

    def fire(self):
        self.Armature.playAction(
            'Barrels_fire', 1, 10, 0, 0, speed = 4,
            play_mode=bgez.logic.KX_ACTION_MODE_PLAY,
            blend_mode=bgez.logic.KX_ACTION_BLEND_ADD
        )
        start = self.Ref_Barrels.worldPosition.copy()
        vect = mathutils.Vector((0, self.range, 0))
        end = start + self.Ref_Barrels.getAxisVect(vect)
        obj, hit, normal = self.Ref_Barrels.rayCast(end, dist=self.range, prop='enemy')
        if isinstance(obj, Damageable):
            if obj.hit(self) is None:
                self.target = None


class Turret_Gatling_BP(Asset, Collider, Deployable):
    REFERENCE = './TowerShooter/Entities/Turrets/Turret_Gatling/Turret_Gatling:Turret_Gatling_BP'

    def construct(self):
        Hitbox = self.getChildrenByName('Turret_Gatling_HB_BP')[0]
        super().construct(Hitbox)
        self.price = 5

    def deploy(self, owner):
        collisions = self.get_current_collisions()
        if not collisions:
            return False
        for object, position, normal in collisions:
            if self.BUILDABLE not in object.getPropertyNames():
                return False
        turret = self.scene.addAsset(Turret_Gatling, reference=self)
        turret.owner = owner
        return True

