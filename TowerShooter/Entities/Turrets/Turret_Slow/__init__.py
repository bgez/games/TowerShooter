from bgez.framework.types import Asset, GameObject
from bgez.framework.objects import Input
from bgez.bases import Mapped
from bgez import utils

from TowerShooter.Entities.Enemy import Enemy
from TowerShooter.Entities import Deployable, Collider

import mathutils
import math
import bgez

class Turret_Slow(Asset, GameObject):
    REFERENCE = './TowerShooter/Entities/Turrets/Turret_Slow/Turret_Slow:Turret_Slow'
    REGISTER = True

    def construct(self):
        self.targets_own = []
        self.range = 10
        self.slow  = 0.5

        self.update_targets()

    def update_targets(self):
        targets_own_old = self.targets_own[:]
        self.targets_own.clear()
        for enemy in Enemy.GetInstances():
            if self.getDistanceTo(enemy) <= self.range :
                self.targets_own.append(enemy)
        for target_old in targets_own_old:
            if target_old not in self.targets_own:
                target_old.speed = target_old.SPEED_BASE

    def post_draw(self, scene):
        self.update_targets()
        for target in self.targets_own:
            target.speed = target.SPEED_BASE * (1 - self.slow)

class Turret_Slow_BP(Asset, Collider, Deployable):
    REFERENCE = './TowerShooter/Entities/Turrets/Turret_Slow/Turret_Slow:Turret_Slow_BP'

    def construct(self):
        Hitbox = self.getChildrenByName('Turret_Slow_HB_BP')[0]
        super().construct(Hitbox)
        self.price = 5

    def deploy(self, owner):
        collisions = self.get_current_collisions()
        if not collisions:
            return False
        for object, position, normal in collisions:
            if self.BUILDABLE not in object.getPropertyNames():
                return False
        turret = self.scene.addAsset(Turret_Slow, reference=self)
        turret.owner = owner
        return True

