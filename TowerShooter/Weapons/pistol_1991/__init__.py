from bgez.framework.types import Asset, GameObject

from TowerShooter.Entities import Damageable, Usable

import bgez.render
import mathutils
import math
import bgez

class pistol_1991(Asset, GameObject, Usable):
    REGISTER = True
    REFERENCE = './TowerShooter/Weapons/pistol_1991/pistol_1991:pistol_1991'

    def construct(self):
        self.default_sight = self.getChildrenByName('Ironsight')[0]
        self.sight = self.default_sight
        self.armature = self.getChildrenByName('Armature_1991')[0]
        self.barrel_muzzle = self.getChildrenByName('Barrel_Muzzle')[0]
        self.receiver = self.getChildrenByName('Receiver')[0]
        self.mags = [self.getChildrenByName('Magazine_out')[0],
                     self.getChildrenByName('Magazine_in')[0]]

        self.range = 100
        self.damages = 10
        self.bullets = 9
        self.owner = None
        self.dropped_mag = None


    def get_sight_offset(self):
        return self.sight.localPosition.copy().y

    def shoot(self):
        start = self.barrel_muzzle.worldPosition.copy()
        vect = mathutils.Vector((0, self.range, 0))
        end = start + self.barrel_muzzle.getAxisVect(vect)
        obj, hit, normal = self.rayCast(end, poly=0, mask=1)
        if isinstance(obj, Damageable):
            obj.hit(self)
        self.bullets -= 1

        action = 'recoil_1991' if self.bullets > 1 else 'recoil_1991_last'
        self.armature.playAction(
            action, 1, 14, 1, speed=4,
            play_mode=bgez.logic.KX_ACTION_MODE_PLAY,
            blend_mode=bgez.logic.KX_ACTION_BLEND_ADD
        )

    def use(self):
        if not self.armature.isPlayingAction(1):
            if self.bullets > 0:
                self.shoot()
            else:
                self.reload()

    def reload(self):
        self.armature.playAction(
            'reload', 1, 80, 1, speed=1.5,
            play_mode=bgez.logic.KX_ACTION_MODE_PLAY,
            blend_mode=bgez.logic.KX_ACTION_BLEND_ADD
        )
        self.mags[0].playAction(
            'mag_out', 1, 30, 1, speed=1.5,
            play_mode=bgez.logic.KX_ACTION_MODE_PLAY,
            blend_mode=bgez.logic.KX_ACTION_BLEND_ADD
        )
        self.mags[1].playAction(
            'mag_in', 1, 56, 1, speed=1.5,
            play_mode=bgez.logic.KX_ACTION_MODE_PLAY,
            blend_mode=bgez.logic.KX_ACTION_BLEND_ADD
        )

    def draw_aim_line(self):
        start = self.barrel_muzzle.worldPosition.copy()
        laser = mathutils.Vector((0, self.range, 0))
        end = start + self.barrel_muzzle.getAxisVect(laser)

        bgez.render.drawLine(start, end, [0, 255, 0])

    def post_draw(self, scene):
        if self.mags[0].getActionFrame(1) == 30:
            self.mags[0].stopAction(1)
            self.dropped_mag = scene.addObject('Magazine', self.mags[0], 800)
            self.dropped_mag.worldOrientation = self.mags[0].worldOrientation.copy()
            self.dropped_mag.setLinearVelocity((0,0,-3), True)
            self.mags[0].worldPosition = self.receiver.worldPosition.copy()
        if self.mags[1].getActionFrame(1) == 56:
            self.mags[1].stopAction(1)
            self.bullets = 9
