from bgez.framework.types import GameObject, Asset

from bgez import utils
import bgez

from TowerShooter.Entities import Damageable, Usable, Collider

class Katana(Asset, Collider, Usable):
    REFERENCE = './TowerShooter/Weapons/Katana/Katana:Katana'
    
    def construct(self):
        Hitbox = self.getChildrenByName('Katana_HB')[0]
        super().construct(Hitbox)
        self.owner = None
        self.damages = 20

    def use(self):
        # Old 1->30 or New 30 -> 67
        if not self.isPlayingAction(0):
            self.playAction(
                'slice', 30, 67, 0, 0, speed=3.5,
                play_mode=bgez.logic.KX_ACTION_MODE_PLAY,
                blend_mode=bgez.logic.KX_ACTION_BLEND_ADD
            )
            self.start_detection(self.isPlayingAction, 0, value=False)

    def on_collision(self, obj, pos, normal):
        if isinstance(obj, Damageable) and obj is not self.owner:
            obj.hit(self)
