from bgez.framework import events
from bgez import Service
from bgez import utils

from bgez.framework.libraries import AssetLibrary
from bgez.framework.objects import Stage

from TowerShooter.Entities.Player import *
from TowerShooter.Entities.Enemy import *
from TowerShooter.Weapons.pistol_1991 import *
from TowerShooter.Weapons.Katana import *
from TowerShooter.World.Nexus import *
from TowerShooter import Entities
from TowerShooter import World
from TowerShooter.GUI import *

import mathutils

import bgui
import bgui.bge_utils
import bgez

@Service.StageManager.MainStage
class MainStage(Stage):
    ASYNC = False

    DEPENDENCIES = [
        AssetLibrary('./TowerShooter/Maps/TestMap', async=ASYNC),
        pistol_1991.Library(async=ASYNC),
        Katana.Library(async=ASYNC),

        Player.Library(async=ASYNC),
        Enemy.Library(async=ASYNC),

        Entities.Turrets.Turret_Slow.Turret_Slow.Library(async=ASYNC),
        Entities.Turrets.Turret_Gatling.Turret_Gatling.Library(async=ASYNC),
    ]

    WAVES = [
        (0, [Enemy] * 5),
        (15, [Enemy] * 15),
        (10, [Enemy] * 30),
        (20, [Enemy] * 60),
        (40, [Enemy] * 500),
    ]

    async def waves(self):
        for i, (delay, enemies) in enumerate(self.WAVES):
            await events.skip(seconds=delay)
            await World.Spawner.Spawner.RandomSpawn(enemies,
                level=i+1, delay=1)

    def start(self, scene):
        Entities.Enemy.Enemy.GRID.clear()

    def ready(self, scene):
        Service.EntityManager.autoLink(*scene.objects)

        self.spawn_player(scene)
        self.nexus = World.Nexus.Nexus.REGISTRY[1]

        self.gui = bgui.bge_utils.System('./TowerShooter/GUI/default')
        self.gui.load_layout(HUD, None)

        Enemy.GRID.feed(scene.objects)
        Enemy.GRID.buildAll()

        task = events.CoroutineTask(self.waves())
        events.handler.run(task)

    def post_draw(self, scene):

        for coord, node in Enemy.GRID.items():

            vertical = mathutils.Vector((0, 0, 1))
            position = Enemy.GRID.toWorld(*coord)

            distance = list(node.values())[0]
            utils.drawVect(position, vertical * distance, (1, 1, 1))

        if self.loaded:
            if self.player.health > 0:
                self.gui.layout.lbl_gold.text = str(self.player.gold) + ' golds'
            else:
                self.gui.layout.lbl_gold.text = 'You died !'
                self.player.controls.disable()
            self.gui.layout.player_health.percent = self.player.health / self.player.MAX_HEALTH
            self.gui.layout.nexus_health.percent = self.nexus.health / self.nexus.MAX_HEALTH
            self.gui.layout.lbl_ammo.text = str(self.player.getAmmo())
            if self.nexus.health == 0 or self.player.health == 0:
                self.gui.layout.lbl_main.text = "Looser Looser No Chicken Dinner !"
            sun = scene.objects.get('Sun')
            sun.worldPosition = scene.active_camera.worldPosition + sun.getAxisVect((0, 0, 32))

    def spawn_player(self, scene):
        self.player = scene.addAsset(Player)
        self.player.PlayerCamera1.setCamera()
        self.player.controls.bind(self.player)
        self.player.controls_cam.bind(self.player)
        self.player.controls_cam.disable()

        self.player.items.append([scene.addAsset(Katana, self.player.back_snap), 
                                self.player.back_snap])
        self.player.items.append([scene.addAsset(pistol_1991, self.player.holster_snap), 
                                self.player.holster_snap])
        for i in range(2):
            self.player.items[i][0].setParent(self.player.items[i][1])
            self.player.items[i][0].owner = self.player
